var req = require("./lib/logger.js");
module.exports = {
  error : req.loggerWithException.error,
  warn : req.logger.warn,
  info : req.logger.info,
  debug : req.logger.debug,
  silly : req.logger.silly,
  verbose : req.logger.verbose,
};
